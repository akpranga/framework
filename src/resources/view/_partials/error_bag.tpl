{if isset($messages)}
    <div class="row">
        <div class="row">
            {foreach from=$messages item=message}
                <div class="col-12">
                    <div class="alert alert-info" role="alert">
                        {$message}
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
{/if}

{if isset($errors)}
    <div class="row">
        <div class="row">
            {foreach from=$errors item=error}
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        {$error}
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
{/if}