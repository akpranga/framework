<form method="post" action="/district/{$district.id}/delete" onsubmit="return confirm('Czy na pewno chcesz usunąć?');">
    <button class="btn btn-danger">
        <i class="fas fa-trash-alt"></i>
    </button>
</form>