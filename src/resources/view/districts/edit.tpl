{extends file="../base.tpl"}

{block name="title"}Edycja dzielnicy: {$district.name}{/block}

{block name="body"}
    <div class="container">
        <div class="row">
            <h1>Edycja dzielnicy: {$district.name}</h1>
        </div>

        {include file="../_partials/error_bag.tpl"}

        <div class="row">
            <form method="post">
                <div class="form-group">
                    <label for="name">Nazwa dzielnicy:</label>
                    <input id="name" type="text" name="name" class="form-control" value="{$district.name}"/>
                </div>
                <div class="form-group">
                    <label for="population">Liczba mieszkańców:</label>
                    <input id="population" type="text" name="population" class="form-control" value="{$district.population}">
                </div>
                <div class="form-group">
                    <label for="city">Miasto:</label>
                    <select id="city" name="city" class="form-control">
                        <option value="Gdańsk" {if $district.city === "Gdańsk"}selected{/if}>Gdańsk</option>
                        <option value="Kraków" {if $district.city === "Kraków"}selected{/if}>Kraków</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="surface">Powierzchnia:</label>
                    <input id="surface" type="text" name="surface" class="form-control" value="{$district.surface}" />
                </div>
                <button type="submit" class="btn btn-primary">Zapisz</button>
            </form>
        </div>
    </div>
{/block}