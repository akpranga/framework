{extends file="base.tpl"}

{block name="title"}Dzielnica: {$district.name}{/block}

{block name="body"}
<div class="container">
    <div class="row">
        <h1>Dzielnica: {$district.name}</h1>
    </div>

    <div class="row">
        <table class="table">
            <tbody>
            <tr>
                <th>Id</th>
                <td>{$district.id}</td>
            </tr>
            <tr>
                <th>Nazwa dzielnicy</th>
                <td>{$district.name}</td>
            </tr>
            <tr>
                <th>Populacja</th>
                <td>{$district.population}</td>
            </tr>
            <tr>
                <th>Miasto</th>
                <td>{$district.city}</td>
            </tr>
            <tr>
                <th>Powierzchnia</th>
                <td>{$district.surface}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-3">
            <a class="btn btn-info" href="/">
                <i class="fas fa-chevron-circle-left"></i> Wróc do listy
            </a>
        </div>

        <div class="col-3">
            <a class="btn btn-primary" href="/district/{$district.id}">
                <i class="fas fa-edit"></i> Edytuj
            </a>
        </div>

        <div class="col-3">
            {include file="districts/_delete_form.tpl" district=$district}
        </div>
    </div>
</div>
{/block}