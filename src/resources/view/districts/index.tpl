{extends file='../base.tpl'}

{block name="title"}Lista dzielnic{/block}

{block name="body"}
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="float-left">
                <h1>Dzielnice</h1>
            </div>
            <div class="float-right">
                <a href="/district/create">
                    <i class="fas fa-plus-square"></i> Dodaj nową dzielnice
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nazwa dzielnicy</th>
                <th>Liczba ludności</th>
                <th>Miasto</th>
                <th>Powierzchnia (km2)</th>
                <th>Akcje</th>
            </tr>
            </thead>
            <tbody>
            {foreach from=$districts item=district}
                <tr>
                    <td>{$district.id}</td>
                    <td>{$district.name}</td>
                    <td>{$district.population}</td>
                    <td>{$district.city}</td>
                    <td>{$district.surface}</td>
                    <td>
                        <a class="btn btn-info" href="/district/{$district.id}/show">
                        <i class="fas fa-eye"></i>
                        </a>
                        <a class="btn btn-primary" href="/district/{$district.id}">
                            <i class="fas fa-edit"></i>
                        </a>
                        {include file="districts/_delete_form.tpl" district=$district}
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="6">brak rekordów</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
{/block}