<?php

if (! function_exists('base_path')) {
    function base_path(string $suffix): string
    {
        return __DIR__ . '/' . $suffix;
    }
}

if (! function_exists('storage_path')) {
    function storage_path(string $suffix): string
    {
        return env('STORAGE_PATH', __DIR__ . '/storage') . '/' . $suffix;
    }
}

if (! function_exists('app_deploy')) {
    function app_deploy(): string
    {
        return config('app.deploy');
    }
}

if (! function_exists('database_path')) {
    function database_path(string $suffix): string
    {
        return env('DATABASE_PATH', __DIR__ . '/database') . '/' . $suffix;
    }
}
