<?php

return [
    'deploy' => env('DEPLOY', 'local'),
    'debug'  => env('DEBUG', true)
];