<?php

use App\Controller\DistrictController;
use FastRoute\RouteCollector;

/** @var Di\Container $container */
$container = require __DIR__ . '/../app/bootstrap.php';

$request = $container->get(\Psr\Http\Message\ServerRequestInterface::class);

$dispatcher = FastRoute\simpleDispatcher(function (RouteCollector $r) {
    $r->addRoute('GET', '/', [DistrictController::class, 'index']);
    $r->addRoute('GET', '/district/create', [DistrictController::class, 'create']);
    $r->addRoute('POST', '/district/create', [DistrictController::class, 'store']);
    $r->addRoute('GET', '/district/{district}', [DistrictController::class, 'edit']);
    $r->addRoute('POST', '/district/{district}', [DistrictController::class, 'update']);
    $r->addRoute('POST', '/district/{district}/delete', [DistrictController::class, 'delete']);
    $r->addRoute('GET', '/district/{district}/show', [DistrictController::class, 'show']);
});

$route = $dispatcher->dispatch($request->getMethod(), $request->getUri()->getPath());

switch ($route[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo '404 Not Found';
        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        echo '405 Method Not Allowed';
        break;

    case FastRoute\Dispatcher::FOUND:
        [, $controller, $parameters] = $route;
        $container->call($controller, $parameters);
        break;
}
