<?php

/** @var \Psr\Container\ContainerInterface $container */
$container = require __DIR__ . '/app/bootstrap.php';

$app = new Silly\Application();

// Silly will use PHP-DI for dependency injection based on type-hints
$app->useContainer($container, $injectWithTypeHint = true);

$app->addCommands([
    new \App\Command\Districts($container->get(\App\Repository\DistrictRepository::class))
]);

$app->run();
