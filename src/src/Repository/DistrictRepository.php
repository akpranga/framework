<?php

namespace App\Repository;

use App\Core\Repository\AbstractRepository;
use App\Model\District;
use App\Services\Importer\Models\District as ScrapedDistrict;

class DistrictRepository extends AbstractRepository
{
    public function __construct(District $model)
    {
        parent::__construct($model);
    }

    /**
     * @param ScrapedDistrict $district
     */
    public function createFromScrapper(ScrapedDistrict $district): void
    {
        $district = $district->toArray();

        $entity = $this->model->where('name', $district['name'])->first();
        if ($entity === null) {
            $entity = new District();
        }

        $entity->fill($district);
        $entity->save();
    }

    /**
     * Create new entity
     *
     * @param array $data
     *
     * @return bool
     */
    public function create(array $data): bool
    {
        $entity = new District();

        $entity->fill($data);

        return $entity->save();
    }
}
