<?php

namespace App\Controller;

use App\Core\ErrorBag\ErrorBag;
use App\Core\View\Contracts\ViewInterface;
use App\Repository\DistrictRepository;
use Psr\Http\Message\ServerRequestInterface;
use Sirius\Validation\Validator;

class DistrictController
{
    /**
     * @var DistrictRepository
     */
    private $repository;

    private $rules = [
        'name:Nazwa dzielnicy'          => 'required',
        'city:Miasto'                   => 'required',
        'surface:Powierzchnia'          => 'required',
        'population:Liczba mieszkańców' => 'required'
    ];

    /**
     * @var ViewInterface
     */
    private $view;

    public function __construct(DistrictRepository $repository, ViewInterface $view)
    {
        $this->repository = $repository;
        $this->view       = $view;
    }

    public function index(): void
    {
        echo $this->view->render('districts/index.tpl', [
            'districts' => $this->repository->getAll(),
        ]);
    }

    public function show($district): void
    {
        $entity = $this->repository->findOneById($district);
        if (! $entity) {
            echo '404 Not Found';

            return;
        }

        echo $this->view->render('districts/show.tpl', [
            'district' => $entity
        ]);
    }

    public function create(): void
    {
        echo $this->view->render('districts/create.tpl');
    }

    public function store(ServerRequestInterface $request): void
    {
        $validator = new Validator();
        $validator->add($this->rules);
        if (! $validator->validate($request->getParsedBody())) {
            echo $this->view->render('districts/create.tpl', [
                'errors' => ErrorBag::createFromValidator($validator->getMessages())
            ]);

            return;
        }

        if ($this->repository->create($request->getParsedBody())) {
            echo $this->view->render('districts/create.tpl', [
                'messages' => ['Nowa dzielnica została dodana!']
            ]);
        }
    }

    public function edit($district): void
    {
        $entity = $this->repository->findOneById($district);
        if (! $entity) {
            echo '404 Not Found';

            return;
        }

        echo $this->view->render('districts/edit.tpl', [
            'district' => $entity
        ]);
    }

    public function update($district, ServerRequestInterface $request): void
    {
        $entity    = $this->repository->findOneById($district);
        $validator = new Validator();
        $validator->add($this->rules);
        if (! $entity) {
            echo '404 Not Found';

            return;
        }

        if (! $validator->validate($request->getParsedBody())) {
            echo $this->view->render('districts/edit.tpl', [
                'errors'   => ErrorBag::createFromValidator($validator->getMessages()),
                'district' => $entity
            ]);

            return;
        }

        if ($this->repository->update($entity, $request->getParsedBody())) {
            header('Location: /district/' . $entity->id . '/show');
        }
    }

    public function delete($district): void
    {
        $entity = $this->repository->findOneById($district);

        if (! $entity) {
            echo '404 Not Found';

            return;
        }

        $this->repository->delete($entity);

        header('Location: /');
    }
}
