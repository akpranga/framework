<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class District
 * @package App\Model
 * @property int $id
 * @property string $name
 * @property string $population
 * @property string $city
 * @property string $surface
 */
class District extends Model
{
    protected $fillable = [
        'name',
        'population',
        'city',
        'surface'
    ];
}
