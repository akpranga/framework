<?php

namespace App\Command;

use App\Repository\DistrictRepository;
use App\Services\Importer\CityFactory;
use App\Services\Importer\Contracts\DistrictsImportInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Districts extends Command
{
    protected static $defaultName = 'app:synchronize';

    private $repository;

    public function __construct(DistrictRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct(null);
    }

    protected function configure(): void
    {
        $this
            ->setName('app:synchronize')
            ->setDescription('Get all district for city')
            ->addArgument('city', InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io   = new SymfonyStyle($input, $output);
        $city = $input->getArgument('city');

        if ($city) {
            try {
                $factory = CityFactory::create($city);
                $this->runImport($factory, $io);
            } catch (NotImplementedCityException $exception) {
                $io->error($exception->getMessage());
            }
        } else {
            $factories = CityFactory::getAllAvailable();
            foreach ($factories as $factoryName => $factory) {
                $this->runImport($factory, $io);
            }
        }

        $io->success('Synchronization has finished');
    }

    private function runImport(DistrictsImportInterface $city, SymfonyStyle $io): void
    {
        $io->note(sprintf('Starting synchronization for city: %s', $city->getCityName()));
        $districts = $city->run();
        foreach ($districts as $district) {
            $io->note(sprintf('Adding district: %s', $district->getName()));
            $this->repository->createFromScrapper($district);
        }
    }
}
