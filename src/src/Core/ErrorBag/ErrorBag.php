<?php

namespace App\Core\ErrorBag;

use Sirius\Validation\ErrorMessage;

final class ErrorBag
{
    /**
     * Convert error bag for Smarty
     *
     * @param array $errors
     *
     * @return array
     */
    public static function createFromValidator(array $errors): array
    {
        return collect($errors)->transform(function ($message) {
            /** @var ErrorMessage $array */
            $array = collect($message)->first();

            return $array->__toString();
        })->toArray();
    }
}
