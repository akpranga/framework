<?php

namespace App\Core\View\Contracts;

interface ViewInterface
{
    public function render(string $view, array $parameters = []);
}
