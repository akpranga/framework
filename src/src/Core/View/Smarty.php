<?php

namespace App\Core\View;

use App\Core\View\Contracts\ViewInterface;
use Smarty as SmartyCore;

class Smarty implements ViewInterface
{
    private $smartyInstance;

    public function __construct(SmartyCore $smartyInstance)
    {
        $this->smartyInstance = $smartyInstance;
        $this->smartyInstance->setCacheDir(storage_path('cache/view/cached'));
        $this->smartyInstance->setCompileDir(storage_path('cache/view/compiled'));
        $this->smartyInstance->setTemplateDir(base_path('resources/view'));
    }

    /**
     * @param string $view
     * @param array $parameters
     *
     * @throws \SmartyException
     */
    public function render(string $view, array $parameters = [])
    {
        $this->smartyInstance->assign($parameters);
        $this->smartyInstance->display($view);
    }
}
