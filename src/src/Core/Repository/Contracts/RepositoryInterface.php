<?php

namespace App\Core\Repository\Contracts;

use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * Create new entity
     *
     * @param array $date
     *
     * @return bool
     */
    public function create(array $date): bool;

    /**
     * Update existing entity
     *
     * @param Model $model
     * @param array $data
     *
     * @return bool
     */
    public function update(Model $model, array $data): bool;

    /**
     * Delete Entity
     *
     * @param Model $model
     *
     * @return bool|null
     */
    public function delete(Model $model): ?bool;
}
