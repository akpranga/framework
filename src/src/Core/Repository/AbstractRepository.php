<?php

namespace App\Core\Repository;

use App\Core\Repository\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class AbstractRepository implements RepositoryInterface
{
    /** @var Model */
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Update existing entity
     *
     * @param Model $model
     * @param array $data
     *
     * @return bool
     */
    public function update(Model $model, array $data): bool
    {
        $model->fill($data);

        return $model->save();
    }

    /**
     * Delete Entity
     *
     * @param Model $model
     *
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Model $model): ?bool
    {
        return $model->delete();
    }

    /**
     * @param string $criteria
     * @param string $operator
     * @param $value
     *
     * @return Collection
     */
    public function findBy(string $criteria, string $operator, $value): Collection
    {
        return $this->model->where($criteria, $operator, $value)->get();
    }

    /**
     * @param int $entitiesId
     *
     * @return Collection
     */
    public function findById(int $entitiesId): Collection
    {
        return $this->findBy($this->model->getKeyName(), '=', $entitiesId);
    }

    /**
     * @param string $criteria
     * @param string $operator
     * @param $value
     *
     * @return Model|null
     */
    public function findOneBy(string $criteria, string $operator, $value): ?Model
    {
        return $this->model->where($criteria, $operator, $value)->first();
    }

    /**
     * @param int $entityId
     *
     * @return Model|null
     */
    public function findOneById(int $entityId): ?Model
    {
        return $this->findOneBy($this->model->getKeyName(), '=', $entityId);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }
}
