<?php

namespace App\Services\Importer\Contracts;

use Illuminate\Support\Collection;

interface DistrictsImportInterface
{
    public function run(): Collection;

    public function getCityName(): string;

    public function getUrlSource(): string;

    public function getFilterXPathForDistrictList(): string;

    public function getBaseUrl(): string;
}
