<?php

namespace App\Services\Importer\Models;

class District
{
    /**
     * @var string
     *
     */
    protected $name;

    /**
     * @var string
     */
    protected $population;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $surface;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return District
     */
    public function setName(string $name): District
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPopulation(): ?string
    {
        return $this->population;
    }

    /**
     * @param string $population
     *
     * @return District
     */
    public function setPopulation(string $population): District
    {
        $this->population = filter_var($population, FILTER_SANITIZE_NUMBER_INT);

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return District
     */
    public function setCity(string $city): District
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurface(): ?string
    {
        return $this->surface;
    }

    /**
     * @param string $surface
     *
     * @return District
     */
    public function setSurface(string $surface): District
    {
        $surface = str_replace(',', '.', $surface);
        $this->surface = filter_var($surface, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        return $this;
    }

    public function toArray(): array
    {
        return [
            'city' => $this->getCity(),
            'surface' => $this->getSurface(),
            'population' => $this->getPopulation(),
            'name' => $this->getName()
        ];
    }
}
