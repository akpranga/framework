<?php

namespace App\Services\Importer\Endpoints;

use App\Services\Importer\Models\District;
use Goutte\Client;
use Illuminate\Support\Collection;
use Symfony\Component\DomCrawler\Crawler;

class GdanskCity extends AbstractCity
{
    public function getBaseUrl(): string
    {
        return 'https://www.gdansk.pl';
    }

    public function getUrlSource(): string
    {
        return 'https://www.gdansk.pl/dzielnice';
    }

    public function getFilterXPathForDistrictList(): string
    {
        return 'div.lista-dzielnic > ul.dropdown-menu > li';
    }

    public function getCityName(): string
    {
        return 'Gdańsk';
    }

    /**
     * @return Collection
     */
    public function run(): Collection
    {
        $crawler   = new Client();
        $crawler   = $crawler->request('GET', $this->getUrlSource());
        $crawler->filter($this->getFilterXPathForDistrictList())->each(function (Crawler $node) {
            $districtsName   = trim($node->text());

            $districtUrl     = $node->filter('a')->attr('href');
            $districtCrawler = new Client();
            $district        = new District();
            $district->setCity($this->getCityName());
            $districtCrawler = $districtCrawler->request('GET', $this->getBaseUrl() . '/' . $districtUrl);
            $districtCrawler->filter('div.opis > div')->each(function (Crawler $node) use ($district) {
                $data = explode(':', trim($node->text()));
                if (\count($data) === 2) {
                    switch (trim($data[0])) {
                        case 'Powierzchnia':
                            $district->setSurface($data[1]);
                            break;
                        case 'Liczba ludności':
                            $district->setPopulation($data[1]);
                            break;
                    }
                } else {
                    $district->setName(trim($node->text()));
                }
            });

            // Remove invalid "district"
            if ($districtsName !== 'Wszystkie') {
                $this->addDistrict($districtsName, $district);
            }
        });

        return $this->districts;
    }
}
