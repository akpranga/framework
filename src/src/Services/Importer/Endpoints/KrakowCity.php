<?php

namespace App\Services\Importer\Endpoints;

use App\Services\Importer\Models\District;
use Goutte\Client;
use Illuminate\Support\Collection;
use Symfony\Component\DomCrawler\Crawler;

class KrakowCity extends AbstractCity
{
    public function getBaseUrl(): string
    {
        return 'http://appimeri.um.krakow.pl/app-pub-dzl/pages';
    }

    public function getUrlSource(): string
    {
        return 'http://appimeri.um.krakow.pl/app-pub-dzl/pages/DzlViewAll.jsf?a=1&lay=normal&fo=0';
    }

    public function getFilterXPathForDistrictList(): string
    {
        return '#mainDiv > map > area';
    }

    public function getCityName(): string
    {
        return 'Kraków';
    }

    /**
     * @param string $districtName
     *
     * @return string
     */
    private function getDistrictName(string $districtName): string
    {
        $pattern = '/([-a-zA-ZąĄćĆęĘłŁńŃóÓśŚźŹżŻ]+ [-a-zA-ZąĄćĆęĘłŁńŃóÓśŚźŹżŻ]+)$|([-a-zA-ZąĄćĆęĘłŁńŃóÓśŚźŹżŻ]+)$/u';
        preg_match($pattern, $districtName, $matches);

        return $matches[0];
    }

    /**
     * @param $surface
     *
     * @return int|mixed
     */
    private function convertSurfaceFromHaToKm($surface)
    {
        $surface = str_replace(',', '.', $surface);
        $surface = filter_var($surface, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        // 1km2 = 100 ha
        $surface /= 100;

        return $surface;
    }

    public function run(): Collection
    {
        $crawler = new Client();
        $crawler = $crawler->request('GET', $this->getUrlSource());
        $crawler->filter($this->getFilterXPathForDistrictList())->each(function (Crawler $node, $i) {
            $districtUrl     = $node->attr('href');
            $districtCrawler = new Client();
            $district        = new District();
            $district->setCity($this->getCityName());
            $districtCrawler = $districtCrawler->request('GET', $this->getBaseUrl() . '/' . $districtUrl);

            $districtName = $this->getDistrictName(trim($districtCrawler->filter('#mainDiv > center > h3')->text()));
            $district->setName($districtName);
            $districtCrawler->filter('#mainDiv > table > tr > td > table > tr')->each(
                function (Crawler $node, $i) use ($district) {
                    $header = $node->filter('tr > td')->text();
                    $value  = trim(preg_replace(
                        '/[\s\t\n\r]+/',
                        '',
                        $node->filter('tr > td')->getNode(1)->nodeValue
                    ));
                    switch ($header) {
                        case 'Powierzchnia:':
                            $value = $this->convertSurfaceFromHaToKm($value);
                            $district->setSurface($value);
                            break;
                        case 'Liczba ludnoci:':
                            $district->setPopulation($value);
                            break;
                    }
                }
            );
            $this->addDistrict($districtName, $district);
        });

        return $this->districts;
    }
}
