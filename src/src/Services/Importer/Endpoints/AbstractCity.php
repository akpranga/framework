<?php

namespace App\Services\Importer\Endpoints;

use App\Services\Importer\Contracts\DistrictsImportInterface;
use App\Services\Importer\Models\District;
use Illuminate\Support\Collection;

abstract class AbstractCity implements DistrictsImportInterface
{
    /** @var Collection */
    protected $districts;

    /**
     * AbstractCity constructor.
     */
    public function __construct()
    {
        $this->districts = new Collection();
    }

    /**
     * @return Collection
     */
    public function getDistrictsCollection(): Collection
    {
        return $this->districts;
    }

    /**
     * @param string $key
     * @param District $district
     *
     * @return $this
     */
    public function addDistrict(string $key, District $district): self
    {
        $this->districts->put($key, $district);

        return $this;
    }
}
