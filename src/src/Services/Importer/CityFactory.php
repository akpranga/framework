<?php

namespace App\Services\Importer;

use App\Services\Importer\Endpoints\AbstractCity;
use App\Services\Importer\Endpoints\GdanskCity;
use App\Services\Importer\Endpoints\KrakowCity;
use App\Services\Importer\Exceptions\NotImplementedCityException;

class CityFactory
{
    private const GDANSK = 'gdansk';
    private const KRAKOW = 'krakow';

    /**
     * @param $name
     *
     * @return AbstractCity|null
     * @throws NotImplementedCityException
     */
    public static function create($name): ?AbstractCity
    {
        switch ($name) {
            case self::GDANSK:
                return new GdanskCity();
                break;
            case self::KRAKOW:
                return new KrakowCity();
                break;
            default:
                throw new NotImplementedCityException("Algorithm for city: {$name} not found!");
        }
    }

    /**
     * Get all city
     * @return array
     */
    public static function getAllAvailable(): array
    {
        return [
            self::GDANSK => new GdanskCity(),
            self::KRAKOW => new KrakowCity()
        ];
    }
}
