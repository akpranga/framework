<?php

use function DI\autowire;

return [
    // Configure Smarty view engine
    \App\Core\View\Contracts\ViewInterface::class => autowire(\App\Core\View\Smarty::class),

    \Psr\Log\LoggerInterface::class => function () {
        $log       = new \Monolog\Logger(app_deploy());
        $handler   = new \Monolog\Handler\RotatingFileHandler(storage_path('logs/app.log'));
        $formatter = new \Monolog\Formatter\LineFormatter(null, null, true, true);
        $handler->setFormatter($formatter);

        $log->pushHandler($handler);

        return $log;
    },

    \App\Repository\DistrictRepository::class       => autowire(\App\Repository\DistrictRepository::class),
    \Psr\Http\Message\ServerRequestInterface::class => \GuzzleHttp\Psr7\ServerRequest::fromGlobals(),

];
