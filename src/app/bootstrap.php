<?php // phpcs:disable PSR1.Files.SideEffects.FoundWithSymbols

use DI\ContainerBuilder;
use Illuminate\Database\Capsule\Manager as Capsule;

require_once __DIR__ . '/../vendor/autoload.php';

(new \Dotenv\Dotenv(__DIR__ . '/../'))->overload();

function config(string $var, $default = null)
{
    static $config = null;
    if (! $config) {
        $config = \Gestalt\Configuration::load(new \Gestalt\Loaders\PhpDirectoryLoader(base_path('config')));
    }

    return $config->get($var, $default);
}

$capsule = new Capsule();
$capsule->addConnection([
    'driver' => 'sqlite',
    'database' => config('database.connections.sqlite.database'),
    'prefix' => config('database.connections.sqlite.prefix')
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

function container(): \Psr\Container\ContainerInterface
{
    static $container = null;

    if (! $container) {
        $builder = new ContainerBuilder();
        if (! config('app.debug')) {
            $builder->enableCompilation(storage_path('cache/di'));
            $builder->writeProxiesToFile(true, storage_path('cache'));
        }
        $builder->addDefinitions(__DIR__ . '/config.php');
        $container = $builder->build();
    }

    return $container;
}

$whoops = new \Whoops\Run();
if (\Whoops\Util\Misc::isCommandLine()) {
    $whoops->pushHandler(new \Whoops\Handler\PlainTextHandler());
} elseif (\Whoops\Util\Misc::isAjaxRequest()) {
    $whoops->pushHandler(new \Whoops\Handler\JsonResponseHandler());
} else {
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
}

$whoops->pushHandler(new \Whoops\Handler\CallbackHandler(function ($exception, $inspector, $run) {
    $logger = container()->get(\Psr\Log\LoggerInterface::class);
    $logger->critical($exception->getMessage(), ['exception' => $exception]);
}));
$whoops->register();

return container();
