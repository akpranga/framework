Aplikacje można uruchomić przy pomocy dockera:

1) Wystarczy uruchomić dockera przy pomocy komendy w terminalu/konsoli:
    
        docker-compose up -d --build
2) Zalogować się do container'a php:
    
        docker-compose run php bash

3) Właczenie synchronizacji należy wykonać komenda:
           
       php console.php app:synchronize
   To polecenie wykona synchronizacje dla obu miast (Gdańsk i Kraków), aby wykonać dla wskazanego miasta należy podać nazwe miasta (bez polskich znaków, małymi literami) jako argument np.
       
       php console.php app:synchronize gdansk
       php console.php app:synchronize krakow
   Link do podglądu strony (CRUD):
   
       http://localhost
    